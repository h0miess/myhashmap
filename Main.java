import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        myHashMap<String, Integer> mymap = new myHashMap<>();
        mymap.put("hello", 3);
        System.out.println(mymap.put("hello", 5));
        mymap.put("KING", 10);
        mymap.put("BLAKE", 99);
        mymap.put("BLAKE", 97);
        mymap.put("WARD", 231);
        System.out.println(mymap);
        System.out.println("size: " + mymap.size());

        System.out.println();
        System.out.println("Get:");
        System.out.println(mymap);
        System.out.println("KING=" + mymap.get("KING"));
        System.out.println("BLAKE=" + mymap.get("BLAKE"));
        System.out.println("WARD=" + mymap.get("WARD"));
        System.out.println("hello=" + mymap.get("hello"));
        System.out.println("d=" + mymap.get("d"));

        System.out.println();
        System.out.println("Contains key:");
        System.out.println(mymap);
        System.out.println("KING = " + mymap.containsKey("KING"));
        System.out.println("BLAKE = " + mymap.containsKey("BLAKE"));
        System.out.println("WARD = " + mymap.containsKey("WARD"));
        System.out.println("hello = " + mymap.containsKey("hello"));
        System.out.println("d = " + mymap.containsKey("d"));

        System.out.println();
        System.out.println("Contains value");
        System.out.println(mymap);
        System.out.println("5 (hello) = " + mymap.containsValue(5));
        System.out.println("10 (KING) = " + mymap.containsValue(10));
        System.out.println("97 (BLAKE) = " + mymap.containsValue(97));
        System.out.println("231 (WARD) = " + mymap.containsValue(231));
        System.out.println("753498 (Not in HashMap) = " + mymap.containsValue(753498));

        System.out.println();
        System.out.println("Remove: ");
        System.out.println(mymap);
        System.out.println(mymap.remove("BLAKE"));
        System.out.println(mymap);

        System.out.println();
        System.out.println("Key set: ");
        System.out.println(mymap.keySet());

        System.out.println();
        System.out.println("Values: ");
        System.out.println(mymap.values());

        System.out.println();
        System.out.println("Clear: ");
        System.out.println(mymap);
        mymap.clear();
        System.out.println(mymap);
        System.out.println("Empty = " + mymap.isEmpty());

    }
}