import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class myHashMap<K, V> {
    private final int INIT_CAPACITY = 16;
    private int capacity = INIT_CAPACITY;
    private Object[] table = new Object[capacity];
    private int size = 0;

    private int hash(K key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    public V put(K key, V value) {
        int h = hash(key);
        int index = h & (capacity - 1);
        Node<K, V> p = (Node<K, V>) table[index];

        if(p == null) {
            table[index] = new Node<>(h, key, value, null);
            size++;
            return null;
        }
        else {
            Node<K, V> e;
            K k;
            if(p.hash == h && p.key.equals(key)) {
                e = p;
            }
            else {
                while (true) {
                    e = p.next;
                    if (p.next == null) {
                        p.next = new Node<>(h, key, value, null);
                        size++;
                        break;
                    }

                    if (e.hash == h && e.key.equals(key)) {
                        break;
                    }
                    p = e;
                }
            }

            if(e != null) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        return null;
    }

    public V get(K key) {
        int h = hash(key);
        int index = h & (capacity - 1);
        Node<K, V> current = (Node<K, V>) table[index];
        if(current == null) {
            return null;
        }
        else{
            if(current.hash == h && current.key.equals(key)) {
                return current.value;
            }
            else {
                Node <K, V> next;
                while (true) {
                    next = current.next;
                    if(next == null) {
                        return null;
                    }
                    else if(next.hash == h && next.key.equals(key)) {
                        return next.value;
                    }
                    current = next;
                }
            }
        }
    }

    public boolean containsKey(K key) {
        for (Object obj : table) {
            Node<K, V> node = (Node<K, V>) obj;
            int h = hash(key);

            while (node != null) {
                if (node.hash == h && node.key.equals(key)) return true;
                node = node.next;
            }
        }
        return false;
    }

    public boolean containsValue(V value) {
        for(Object obj : table) {
            Node<K, V> node = (Node<K, V>) obj;
            while(node != null) {
                if(node.value.equals(value)) return true;
                node = node.next;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public V remove(K key) {
        int h = hash(key);
        int index = h & (capacity - 1);
        Node<K, V> current = (Node <K, V>) table[index];
        if(current == null) return null;
        else{
            if(current.hash == hash(key) && current.key.equals(key)) {
                V oldVal = current.value;
                if(current.next == null) table[index] = null;
                else table[index] = current.next;
                size--;
                return oldVal;
            }

            while(current.next != null) {
                if(current.next.hash == hash(key) && current.next.key.equals(key)) {
                    V oldVal = current.next.value;
                    if(current.next.next == null) current.next = null;
                    else current.next = current.next.next;
                    size--;
                    return oldVal;
                }
                current = current.next;
            }
            return null;
        }
    }

    public int size() {
        return size;
    }

    public void clear() {
        size = 0;
        for(int i = 0; i < capacity - 1; i++) table[i] = null;
    }

    public List<K> keySet() {
        ArrayList<K> keySet = new ArrayList<>();
        for(Object obj : table) {
            Node<K, V> node = (Node<K, V>) obj;
            if(node == null) continue;
            keySet.add(node.key);
            if(node.next != null) {
                node = node.next;
                while(node != null) {
                    keySet.add(node.key);
                    node = node.next;
                }
            }
        }
        return keySet;
    }

    public List<V> values() {
        ArrayList<V> values = new ArrayList<>();
        for(Object obj : table) {
            Node<K, V> node = (Node<K, V>) obj;
            if(node == null) continue;
            values.add(node.value);
            if(node.next != null) {
                node = node.next;
                while(node != null) {
                    values.add(node.value);
                    node = node.next;
                }
            }
        }
        return values;
    }

    @Override
    public String toString() {

        if(isEmpty()) {
            return "{}";
        }

        StringBuilder result = new StringBuilder("{");

        for(Object obj : table) {
            Node<K, V> node = (Node<K, V>) obj;
            if(node == null) continue;
            result.append(node.key).append("=").append(node.value).append(", ");
            if(node.next != null) {
                node = node.next;
                while (node != null) {
                    result.append(node.key).append("=").append(node.value).append(", ");
                    node = node.next;
                }
            }
        }

        result.replace(result.length() - 2, result.length() - 1, "}");
        return result.toString();
    }

    private static class Node<K, V> {
        final int hash;
        final K key;
        V value;
        Node<K, V> next;

        public Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

    }
}
